const express = require('express');
const logger = require('morgan');
const app = express();
app.use(logger('dev'));
const port = 3000;

var fecha = require('./fecha');

const about = require('./about.js');
app.use('/about', about);



app.get('/', (req, res) => {
    res.send(fecha.fecha().toString())
});

app.listen(port, () => {
    console.log("Servidor corriendo en el puerto " + port)
});

