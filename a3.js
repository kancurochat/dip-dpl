var http = require('http');
var port = 3000;

var calcu = require('./calculadora');
var n1 = 10;
var n2 = 5;

var fecha = require('./fecha');



http.createServer(function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(fecha.fecha().toString());
}).listen(port);
 // EJERCICIO 1.1
console.log("Servidor corriendo en el puerto " + port);
console.log("Números: " + n1 + " y " + n2);
console.log("Suma: " + calcu.suma(n1, n2));
console.log("Resta: " + calcu.resta(n1, n2));
console.log("División: " + calcu.division(n1, n2));
console.log("Multiplicación: " + calcu.multi(n1, n2));
console.log("Módulo: " + calcu.mod(n1, n2));

console.log("\n");
//EJERCICIO 1.2
console.log(fecha.fecha());
