const express = require('express');
const router = express.Router();

router.get('/', function(req, res) {
    res.send('<h1>About me:</h1>' 
    + '<p>Nombre: Daniel Infante Pacheco</p>' 
    + '<p>Edad: 20 años</p>' 
    + '<p>Comida favorita: Cualquier cosa que me prepare yo</p>' );
});

module.exports = router;